import pygame
from pygame.locals import *
import sys

def load_image(name):
    image = pygame.image.load(name)
    return image

class TalkSprite(pygame.sprite.Sprite):
    def __init__(self):
        super(TalkSprite, self).__init__()
        self.images = []
        self.images.append(load_image('/root/AlexaPi/frames/talk__0000_Layer-11.png'))
        self.images.append(load_image('/root/AlexaPi/frames/talk__0001_Layer-10.png'))
        self.images.append(load_image('/root/AlexaPi/frames/talk__0002_Layer-9.png'))
        self.images.append(load_image('/root/AlexaPi/frames/talk__0003_Layer-8.png'))
        self.images.append(load_image('/root/AlexaPi/frames/talk__0004_Layer-7.png'))
        self.images.append(load_image('/root/AlexaPi/frames/talk__0005_Layer-6.png'))
        #self.images.append(load_image('/root/AlexaPi/frames/talk__0006_Layer-5.png'))
        #self.images.append(load_image('/root/AlexaPi/frames/talk__0007_Layer-4.png'))
        #self.images.append(load_image('/root/AlexaPi/frames/talk__0008_Layer-3.png'))
        #self.images.append(load_image('/root/AlexaPi/frames/talk__0009_Layer-2.png'))
        #self.images.append(load_image('/root/AlexaPi/frames/talk__0010_Layer-1.png'))
        # assuming all images are 498x280 pixels

        self.index = 0
        self.image = self.images[self.index]
        #self.rect = pygame.Rect(5, 5, 498, 280)
        self.rect = pygame.Rect(6, 400, 498, 280)

    def update(self):
        '''This method iterates through the elements inside self.images and 
        displays the next one each tick. For a slower animation, you may want to 
        consider using a timer of some sort so it updates slower.'''
        self.index += 1
        if self.index >= len(self.images):
            self.index = 0
        self.image = self.images[self.index]

class StartSprite(pygame.sprite.Sprite):
    def __init__(self):
        super(StartSprite, self).__init__()
        self.images = []
        self.images.append(load_image('/root/AlexaPi/frames/start__0000_Layer-38.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0001_Layer-37.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0002_Layer-36.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0003_Layer-35.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0004_Layer-34.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0005_Layer-33.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0006_Layer-32.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0007_Layer-31.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0008_Layer-30.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0009_Layer-29.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0010_Layer-28.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0011_Layer-27.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0012_Layer-26.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0013_Layer-25.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0014_Layer-24.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0015_Layer-23.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0016_Layer-22.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0017_Layer-21.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0018_Layer-20.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0019_Layer-19.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0020_Layer-18.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0021_Layer-17.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0022_Layer-16.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0023_Layer-15.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0024_Layer-14.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0025_Layer-13.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0026_Layer-12.png'))
        # assuming all images are 498x280 pixels

        self.index = 0
        self.image = self.images[self.index]
        #self.rect = pygame.Rect(5, 5, 498, 280)
        self.rect = pygame.Rect(6, 400, 498, 280)

    def update(self):
        self.index += 1
        if self.index >= len(self.images):
            self.index = 0
        self.image = self.images[self.index]


def animatecat(my_sprite, nbre):
    pygame.init()
    screen = pygame.display.set_mode()
    my_group = pygame.sprite.Group(my_sprite)

    for x in range(0,nbre):
        event = pygame.event.poll()
        if event.type == pygame.QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
            pygame.quit()
            sys.exit(0)

        # Calling the 'my_group.update' function calls the 'update' function of all 
        # its member sprites. Calling the 'my_group.draw' function uses the 'image'
        # and 'rect' attributes of its member sprites to draw the sprite.
        my_group.update()
        my_group.draw(screen)
        pygame.display.flip()
        pygame.time.wait(80)


def talkingcat():
    pygame.init()
    screen = pygame.display.set_mode()

    my_sprite = TalkSprite()
    my_group = pygame.sprite.Group(my_sprite)

    for x in range(0,6):
        event = pygame.event.poll()
        if event.type == pygame.QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
            pygame.quit()
            sys.exit(0)

        my_group.update()
        my_group.draw(screen)
        pygame.display.flip()
        pygame.time.wait(80)
