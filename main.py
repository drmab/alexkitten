import alsaaudio
import json
import os.path
import os
import requests
import re
import sys
import time
import datetime

from creds import *
from pocketsphinx.pocketsphinx import *
from sphinxbase.sphinxbase import *
from threading import Thread

import catalk

# ------ Start User configuration settings --------
sphinx_data_path = "/root/pocketsphinx/"
modeldir = sphinx_data_path+"/model/"
datadir = sphinx_data_path+"/test/data"

recording_file_path = "/root/AlexaPi/"
filename=recording_file_path+"/myfile.wav"
filename_raw=recording_file_path+"/myfile.pcm"

device = "plughw:1" # Name of your microphone/soundcard in arecord -L

# Personalize the robot :)
username = "Stephie"

# Trigger phrase. Pick a phrase that is easy to save repeatedly the SAME way
# seems by default a single syllable word is better
trigger_phrase = "kitten"

# ----- End User Configuration -----

# PocketSphinx configuration
config = Decoder.default_config()

# Set recognition model to US
config.set_string('-hmm', os.path.join(modeldir, 'en-us/en-us'))
config.set_string('-dict', os.path.join(modeldir, 'en-us/cmudict-en-us.dict'))

#Specify recognition key phrase
config.set_string('-keyphrase', trigger_phrase)
config.set_float('-kws_threshold',3)

# Hide the VERY verbose logging information
config.set_string('-logfn', '/dev/null')

path = os.path.realpath(__file__).rstrip(os.path.basename(__file__))

# Read microphone at 16 kHz. Data is signed 16 bit little endian format.
inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL, device)
inp.setchannels(1)
inp.setrate(16000)
inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
inp.setperiodsize(500)

token = None
recording_file = None

start = time.time()

# Determine if trigger word/phrase has been detected
record_audio = False
wit_ai_received = False

# Process audio chunk by chunk. On keyword detected perform action and restart search
decoder = Decoder(config)
decoder.start_utt()

# All Alexa code based on awesome code from AlexaPi
# https://github.com/sammachin/AlexaPi

# Verify that the user is connected to the internet
def internet_on():
	print "Checking Internet Connection"
	try:
		r =requests.get('https://api.amazon.com/auth/o2/token')
		print "Connection OK"
		return True
	except:
		print "Connection Failed"
		return False

#Get Alexa Token
def gettoken():
	global token
	refresh = refresh_token
	if token:
		return token
	elif refresh:
		payload = {"client_id" : Client_ID, "client_secret" : Client_Secret, "refresh_token" : refresh, "grant_type" : "refresh_token", }
		url = "https://api.amazon.com/auth/o2/token"
		r = requests.post(url, data = payload)
		resp = json.loads(r.text)
		token = resp['access_token']
		return token
	else:
		return False
		
def alexa():
	url = 'https://access-alexa-na.amazon.com/v1/avs/speechrecognizer/recognize'
	headers = {'Authorization' : 'Bearer %s' % gettoken()}
        # Set parameters to Alexa request for our audio recording
	d = {
   		"messageHeader": {
       		"deviceContext": [
           		{
               		"name": "playbackState",
               		"namespace": "AudioPlayer",
               		"payload": {
                   		"streamId": "",
        			   	"offsetInMilliseconds": "0",
                   		"playerActivity": "IDLE"
               		}
           		}
       		]
		},
   		"messageBody": {
       		"profile": "alexa-close-talk",
       		"locale": "en-us",
       		"format": "audio/L16; rate=44100; channels=1"
   		}
	}

        # Send our recording audio to Alexa
	with open(filename_raw) as inf:
		files = [
				('file', ('request', json.dumps(d), 'application/json; charset=UTF-8')),
				('file', ('audio', inf, 'audio/L16; rate=44100; channels=1'))
				]	
		r = requests.post(url, headers=headers, files=files)

	if r.status_code == 200:
		print "Debug: Alexa provided a response"

		for v in r.headers['content-type'].split(";"):
			if re.match('.*boundary.*', v):
				boundary =  v.split("=")[1]
		data = r.content.split(boundary)
		for d in data:
			if (len(d) >= 1024):
				audio = d.split('\r\n\r\n')[1].rstrip('--')

                # Write response audio to response.mp3 may or may not be played later
		with open(path+"response.mp3", 'wb') as f:
			f.write(audio)

		#os.system('mpg123 -q {}response.mp3'.format(path))
		get_talking('response')
		'''
    	isTalking = Thread(target=os.system, args=('mpg123 -q {}response.mp3'.format(path),))
    	isTalking.start()

    	while isTalking.isAlive():
	    	catalk.talkingcat()
	    '''
	else:
		print "Debug: Alexa threw an error with code: ",r.status_code


def get_talking(filename):
	isTalking = Thread(target=os.system, args=('mpg123 -q {}{}.mp3'.format(path, filename),))
	isTalking.start()

	while isTalking.isAlive():
    		catalk.talkingcat()	


def offline_speak(string):
	print "We are saying ",string
	os.system('espeak -ven-uk -p50 -s140 "'+string+'" > /dev/null 2>&1')
	
	isTalking = Thread(target=os.system, args=('espeak -ven-uk -p50 -s140 "'+string+'" > /dev/null 2>&1',))
	isTalking.start()

	while isTalking.isAlive():
    	catalk.talkingcat()
    	

while internet_on() == False:
	print "."

catalk.animatecat(catalk.StartSprite(),25)
today = datetime.date.today()
if today.month == 10 and today.day == 21:
	get_talkking('hs')
else:
	get_talking('hshc')
#get_talking('hello')
#offline_speak("Hello "+username+", how can I help you?")

#print "Debug: Ready to receive request"
while True:
	try:
		# Read from microphone
		l,buf = inp.read()
	except:
                # Hopefully we read fast enough to avoid overflow errors
		print "Debug: Overflow"
		continue

        #Process microphone audio via PocketSphinx only when trigger word
        # hasn't been detected
	if buf and record_audio == False:
		decoder.process_raw(buf, False, False)

	# Detect if keyword/trigger word was said
	if record_audio == False and decoder.hyp() != None:
                # Trigger phrase has been detected
		record_audio = True
		start = time.time()

                # To avoid overflows close the microphone connection
		inp.close()

                # Open file that will be used to save raw micrphone recording
		recording_file = open(filename_raw, 'w')
		recording_file.truncate()

		# Indicate that the system is listening to request
		#get_talking('Cat-meow')
		os.system('mpg123 -q {}hello.mp3'.format(path))

        # Reenable reading microphone raw data
		inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL, device)
		inp.setchannels(1)
		inp.setrate(16000)
		inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
		inp.setperiodsize(1024)

		#print ("Debug: Start recording")

	# Only write if we are recording
	if record_audio == True:
		recording_file.write(buf)

	# Stop recording after 5 seconds
	if record_audio == True and time.time() - start > 5:
		#print ("Debug: End recording")
		get_talking('Meow-sound')
		record_audio = False

		# Close file we are saving microphone data to
		recording_file.close()

		# Convert raw PCM to wav file (includes audio headers)
		os.system("sox -t raw -r 16000 -e signed -b 16 -c 1 "+filename_raw+" "+filename+" && sync");

		# Send recording to our speech recognition web services
		alexa()

		# Now that request is handled restart audio decoding
		decoder.end_utt()
		decoder.start_utt()