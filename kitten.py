#! /usr/bin/env python

import os
import random
import time
import RPi.GPIO as GPIO
import alsaaudio
import wave
import random
from creds import *
import requests
import json
import re
from memcache import Client

import catalk
import pygame
from pygame.locals import *

#Settings
button = 18 #GPIO Pin with button connected
lights = [24, 25] # GPIO Pins with LED's conneted
device = "plughw:1" # Name of your microphone/soundcard in arecord -L

#Setup
recorded = False
servers = ["127.0.0.1:11211"]
mc = Client(servers, debug=1)
path = os.path.realpath(__file__).rstrip(os.path.basename(__file__))


def load_image(name):
    image = pygame.image.load(name)
    return image

class TestSprite(pygame.sprite.Sprite):
    def __init__(self):
        super(TestSprite, self).__init__()
        self.images = []
        self.images.append(load_image('/root/AlexaPi/frames/start__0000_Layer-38.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0001_Layer-37.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0002_Layer-36.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0003_Layer-35.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0004_Layer-34.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0005_Layer-33.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0006_Layer-32.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0007_Layer-31.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0008_Layer-30.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0009_Layer-29.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0010_Layer-28.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0011_Layer-27.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0012_Layer-26.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0013_Layer-25.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0014_Layer-24.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0015_Layer-23.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0016_Layer-22.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0017_Layer-21.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0018_Layer-20.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0019_Layer-19.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0020_Layer-18.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0021_Layer-17.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0022_Layer-16.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0023_Layer-15.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0024_Layer-14.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0025_Layer-13.png'))
        self.images.append(load_image('/root/AlexaPi/frames/start__0026_Layer-12.png'))
        # assuming all images are 498x280 pixels

        self.index = 0
        self.image = self.images[self.index]
        self.rect = pygame.Rect(5, 5, 498, 280)

    def update(self):
        '''This method iterates through the elements inside self.images and 
        displays the next one each tick. For a slower animation, you may want to 
        consider using a timer of some sort so it updates slower.'''
        self.index += 1
        if self.index >= len(self.images):
            self.index = 0
        self.image = self.images[self.index]


def internet_on():
    print "Checking Internet Connection"
    try:
        r =requests.get('https://api.amazon.com/auth/o2/token')
	print "Connection OK"
        return True
    except:
	print "Connection Failed"
    	return False

	
def gettoken():
	token = mc.get("access_token")
	refresh = refresh_token
	if token:
		return token
	elif refresh:
		payload = {"client_id" : Client_ID, "client_secret" : Client_Secret, "refresh_token" : refresh, "grant_type" : "refresh_token", }
		url = "https://api.amazon.com/auth/o2/token"
		r = requests.post(url, data = payload)
		resp = json.loads(r.text)
		mc.set("access_token", resp['access_token'], 3570)
		return resp['access_token']
	else:
		return False
		

def alexa():
	#GPIO.output(lights[0], GPIO.HIGH)
	url = 'https://access-alexa-na.amazon.com/v1/avs/speechrecognizer/recognize'
	headers = {'Authorization' : 'Bearer %s' % gettoken()}
	d = {
   		"messageHeader": {
       		"deviceContext": [
           		{
               		"name": "playbackState",
               		"namespace": "AudioPlayer",
               		"payload": {
                   		"streamId": "",
        			   	"offsetInMilliseconds": "0",
                   		"playerActivity": "IDLE"
               		}
           		}
       		]
		},
   		"messageBody": {
       		"profile": "alexa-close-talk",
       		"locale": "en-us",
       		"format": "audio/L16; rate=16000; channels=1"
   		}
	}
	with open(path+'recording.wav') as inf:
		files = [
				('file', ('request', json.dumps(d), 'application/json; charset=UTF-8')),
				('file', ('audio', inf, 'audio/L16; rate=16000; channels=1'))
				]	
		r = requests.post(url, headers=headers, files=files)
	if r.status_code == 200:
		for v in r.headers['content-type'].split(";"):
			if re.match('.*boundary.*', v):
				boundary =  v.split("=")[1]
		data = r.content.split(boundary)
		for d in data:
			if (len(d) >= 1024):
				audio = d.split('\r\n\r\n')[1].rstrip('--')
		with open(path+"response.mp3", 'wb') as f:
			f.write(audio)
		#GPIO.output(lights[1], GPIO.LOW)

    	isTalking = threading.Thread(target=os.system, args=('mpg123 -q {}1sec.mp3 {}response.mp3 {}1sec.mp3'.format(path, path, path),))
    	isTalking.start()

    	while isTalking.isAlive():
	    	catalk.talkingcat()
		#os.system('mpg123 -q {}1sec.mp3 {}response.mp3 {}1sec.mp3'.format(path, path, path))
		#GPIO.output(lights[0], GPIO.LOW)
	"""else:
		GPIO.output(lights[1], GPIO.LOW)
		for x in range(0, 3):
			time.sleep(.2)
			GPIO.output(lights[1], GPIO.HIGH)
			time.sleep(.2)
			GPIO.output(lights[1], GPIO.LOW)
		"""		



def start():
	last = GPIO.input(button)
    pygame.init()
    screen = pygame.display.set_mode()
	pygame.mouse.set_visible(0)

    my_sprite = TestSprite()
    my_group = pygame.sprite.Group(my_sprite)

    for x in range(0,25):
        # Calling the 'my_group.update' function calls the 'update' function of all 
        # its member sprites. Calling the 'my_group.draw' function uses the 'image'
        # and 'rect' attributes of its member sprites to draw the sprite.
        my_group.update()
        my_group.draw(screen)
        pygame.display.flip()
        pygame.time.wait(80)

	while True:
		val = GPIO.input(button)
		GPIO.wait_for_edge(button, GPIO.FALLING) # we wait for the button to be pressed
		#GPIO.output(lights[1], GPIO.HIGH)
		inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL, device)
		inp.setchannels(1)
		inp.setrate(16000)
		inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
		inp.setperiodsize(500)
		audio = ""
		while(GPIO.input(button)==0): # we keep recording while the button is pressed
			l, data = inp.read()
			if l:
				audio += data
		rf = open(path+'recording.wav', 'w')
		rf.write(audio)
		rf.close()
		inp = None
		alexa()

	

if __name__ == "__main__":
	GPIO.setwarnings(False)
	GPIO.cleanup()
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(button, GPIO.IN, pull_up_down=GPIO.PUD_UP)
	GPIO.setup(lights, GPIO.OUT)
	GPIO.output(lights, GPIO.LOW)
	while internet_on() == False:
		print "."
	token = gettoken()
	os.system('mpg123 -q {}1sec.mp3 {}hello.mp3'.format(path, path))
	for x in range(0, 3):
		time.sleep(.1)
		GPIO.output(lights[0], GPIO.HIGH)
		time.sleep(.1)
		GPIO.output(lights[0], GPIO.LOW)
	start()
